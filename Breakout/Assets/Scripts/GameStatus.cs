﻿using UnityEngine;
using UnityEngine.UI;

public class GameStatus : MonoBehaviour
{
    // Config
    public bool isAutoPlay = false;
    [Range(0.1f, 10f)] public float gameSpeed = 1f;
    public int pointPerBlockdestroyed = 100;
    public Text scoreText;

    // State
    public int currentScore = 0;

    public static GameStatus Instace;

    private void Awake()
    {
        if (Instace == null)
        {
            Instace = this;
        }
    }

    void Start()
    {
        UpdateScoreText();
    }

    void Update()
    {
        Time.timeScale = gameSpeed;
    }

    public void AddToScore()
    {
        currentScore += pointPerBlockdestroyed;
        UpdateScoreText();
    }

    void UpdateScoreText()
    {
        scoreText.text = "" + currentScore;
    }
}
