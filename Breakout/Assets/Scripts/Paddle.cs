﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    GameStatus gameStatus;
    Ball ball;
    void Start()
    {
        ball = FindObjectOfType<Ball>();
        gameStatus = GameStatus.Instace;
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameStatus.isAutoPlay)
        {
            var x = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
            var paddlePosition = new Vector2(x, -5.5f);
            paddlePosition.x = Mathf.Clamp(x, -9.5f, 9.5f);
            transform.position = paddlePosition;
        }
        else
        {
            var x = ball.transform.position.x;
            var paddlePosition = new Vector2(x, -5.5f);
            transform.position = paddlePosition;
        }
    }
}
