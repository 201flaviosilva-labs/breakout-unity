﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{
    public int breakableBlocks = 0;

    public void addCountBlock()
    {
        breakableBlocks++;
    }

    public void removeBlockDestroid()
    {
        breakableBlocks--;
        if (breakableBlocks <= 0)
        {
            String sceneName = SceneManager.GetActiveScene().name;
            if (sceneName == "Level 1")
            {
                SceneManager.LoadScene("Level 2");
            }
            else if (sceneName == "Level 2")
            {
                SceneManager.LoadScene("Start");
            }
        }
    }
}
