﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartSceneBtns : MonoBehaviour
{
    public Button btn;
    public bool isQuit;
    // Start is called before the first frame update
    void Start()
    {
        btn = GetComponent<Button>();
        btn.onClick.AddListener(OnClick);
    }

    void OnClick()
    {
        if (isQuit)
        {
            Application.Quit();
        }
        else
        {
            SceneManager.LoadScene("Level 1");
        }
    }
}
