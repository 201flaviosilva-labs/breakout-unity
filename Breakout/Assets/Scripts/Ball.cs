﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Ball : MonoBehaviour
{
    public Paddle paddle1;
    public AudioClip[] ballSounds;


    Rigidbody2D rb2d;
    bool isBallLauched = false;

    Vector2 paddleToBallVector;
    void Start()
    {
        paddleToBallVector = transform.position - paddle1.transform.position;
        rb2d = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (!isBallLauched)
        {
            LockBallToPaddle();
            LauchBallOnMouseClick();
        }
    }

    private void LauchBallOnMouseClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            rb2d.velocity = new Vector2(2f, 15f);
            isBallLauched = true;
        }
    }

    private void LockBallToPaddle()
    {
        Vector2 newBallPos = new Vector2(paddle1.transform.position.x, paddle1.transform.position.y);
        transform.position = newBallPos + paddleToBallVector;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        var randomAudio = ballSounds[Random.Range(0, ballSounds.Length)];
        GetComponent<AudioSource>().PlayOneShot(randomAudio);
    }
}
